package com.epam.model;

public class AreaException extends Exception{
    public AreaException() {
    }

    public AreaException(String message) {
        super(message);
    }

    public AreaException(String message, Throwable cause) {
        super(message, cause);
    }

    public AreaException(Throwable cause) {
        super(cause);
    }

    public AreaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
