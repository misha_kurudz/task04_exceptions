package com.epam.model;

public class Square {
    private int firstSide;
    private int secondSide;

    public Square(int firstSide, int secondSide) {
        this.firstSide = firstSide;
        this.secondSide = secondSide;
    }

    public void setFirstSecondSide(int firstSide, int secondSide) throws AreaException {
        if(firstSide<=0 || secondSide<=0){
            System.out.println("error");
            throw new AreaException("First side or second side can't be smaller than 0");
        }
        this.firstSide = firstSide;
    }

    public int getAreaOfSquare(int firstSide, int secondSide){
        return firstSide*secondSide;
    }

    @Override
    public String toString() {
        return "Square{" +
                "firstSide=" + firstSide +
                ", secondSide=" + secondSide +
                '}';
    }
}
