package com.epam.model;

public class Segment {
    private int start;
    private int end;

    public Segment(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public void setStartEnd(int start, int end) throws LengthException {
        if(start>end){
            System.out.println("error");
            throw new LengthException("Start point can't be greater than the end point");
        }
        this.start = start;
    }



    public int lengthOfSegment(int start, int end){
        return end-start;
    }

    @Override
    public String toString() {
        return "Segment{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }
}
