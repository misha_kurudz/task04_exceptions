package com.epam.controller;


import com.epam.model.AreaException;
import com.epam.model.LengthException;
import com.epam.model.Segment;
import com.epam.model.Square;

public class ControllerImpl implements Controller {
    private Segment segment;
    private Square square;
    public ControllerImpl() {
        segment = new Segment(0,0);
        square = new Square(0,0);
    }
    public int getLengthSegment(int start , int end) throws LengthException {
        try{
            segment.setStartEnd(start,end);
        } catch (LengthException e){
            e.printStackTrace();
            return 0;
        }

        return segment.lengthOfSegment(start,end);
    }

    public int getAreaSquare(int firstSide, int secondSide){
        try{
            square.setFirstSecondSide(firstSide,secondSide);
        } catch (AreaException e){
            e.printStackTrace();
            return 0;
        }
        return square.getAreaOfSquare(firstSide,secondSide);
    }





//
//    @Override
//    public void goToTheNewShop() {
//        shop = new Shop();
//    }
//
//    @Override
//    public List getShopProducts() {
//        return shop.getCleaners();
//    }
//
//    @Override
//    public List sortSelectedCleaners(int type) {
//        List<Cleaner> selected_cleaners = new LinkedList<Cleaner>();
//        selected_cleaners = shop.selectCleaners(type);
//        Collections.sort(selected_cleaners);
//        return selected_cleaners;
//    }
}

