package com.epam.controller;

import com.epam.model.LengthException;

import java.util.List;

public interface Controller {
    int getLengthSegment(int start, int end) throws LengthException;
    int getAreaSquare(int firstSide, int secondSide);
//    void goToTheNewShop();
//    List getShopProducts();
//    List sortSelectedCleaners(int type);
}
