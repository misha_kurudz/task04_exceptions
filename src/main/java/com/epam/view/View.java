package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import com.epam.model.LengthException;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - Length segment");
        menu.put("2", " 2 - Area of square");
        menu.put("Q", " Q - Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::PressButton1);
        methodsMenu.put("2", this::PressButton2);
    }

    private void PressButton1() throws LengthException {
        System.out.println("Input first point:");
        Scanner start = new Scanner(System.in);
        int startPoint = start.nextInt();
        System.out.println("Input second point:");
        Scanner end = new Scanner(System.in);
        int endPoint = end.nextInt();

        int len = controller.getLengthSegment(startPoint,endPoint);
            System.out.println("Length on segment: " + len);
    }


    private void PressButton2() {
        System.out.println("Input length first side:");
        Scanner first = new Scanner(System.in);
        int firstSide = first.nextInt();
        System.out.println("Input length second side:");
        Scanner second = new Scanner(System.in);
        int secondSide = second.nextInt();

        int area = controller.getAreaSquare(firstSide,secondSide);
        System.out.println("Area os square: " + area);
    }

//    private void PressButton2() {
//        System.out.println("input type ( '1' - Dishes, '2' - Floor, '3' - Furniture ): ");
//        int type = input.nextInt();
//        System.out.format("%-20s %-20s %-20s %-20s\n", "Type", "Name", "Producer", "Price");
//        List lst = controller.sortSelectedCleaners(type);
//        for (Object cleaner : lst) {
//            System.out.println(cleaner);
//        }
//    }

    private void outputMenu() {
        System.out.println("\nMenu:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }


    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine();
            try {
                methodsMenu.get(keyMenu).print();

            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}

