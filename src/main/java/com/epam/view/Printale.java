package com.epam.view;


import com.epam.model.LengthException;

@FunctionalInterface
interface Printable {
    void print() throws LengthException;
}
